" Preamble {{{
" ------------------------------------------------------------------------------
"        File: uncrustify.vim
" ------------------------------------------------------------------------------
"     Version: 0.0.0
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ------------------------------------------------------------------------------
" }}} Preamble

if exists('b:did_ftplugin')
  finish
endif

let b:did_ftplugin = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

let &l:comments      = ':#'
let &l:commentstring = '#%s'

let b:undo_ftplugin =
\ 'setlocal comments<'                                                         .
\ ' '                                                                          .
\ '|'                                                                          .
\ ' '                                                                          .
\ 'setlocal commentstring<'

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
