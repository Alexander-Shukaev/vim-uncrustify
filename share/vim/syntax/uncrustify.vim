" Preamble {{{
" ------------------------------------------------------------------------------
"        File: uncrustify.vim
" ------------------------------------------------------------------------------
"     Version: 0.0.0
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ------------------------------------------------------------------------------
" }}} Preamble

if exists('b:current_syntax')
  finish
endif

syntax case ignore

syntax match uncrustify_AssignmentOperator /=/
syntax match uncrustify_Comma              /,/
syntax match uncrustify_LeftBrace          /{/
syntax match uncrustify_RightBrace         /}/

highlight default link uncrustify_AssignmentOperator Delimiter
highlight default link uncrustify_Comma              Delimiter
highlight default link uncrustify_Brace              Delimiter
highlight default link uncrustify_LeftBrace          uncrustify_Brace
highlight default link uncrustify_RightBrace         uncrustify_Brace

syntax match uncrustify_Identifier /\<\w\+\>/

highlight default link uncrustify_Identifier Identifier

syntax keyword uncrustify_BooleanTag       BOOLEAN contained
syntax keyword uncrustify_BooleanConstants TRUE FALSE

highlight default link uncrustify_BooleanTag       Type
highlight default link uncrustify_BooleanConstants Boolean

syntax keyword uncrustify_IntegerTag       INTEGER contained
syntax match   uncrustify_IntegerConstants /\<[-+]\=\d\+\>/

highlight default link uncrustify_IntegerTag       Type
highlight default link uncrustify_IntegerConstants Number

syntax keyword uncrustify_StringTag       STRING contained
syntax match   uncrustify_StringConstants /".*"/

highlight default link uncrustify_StringTag       Type
highlight default link uncrustify_StringConstants String

syntax keyword uncrustify_EnumerationTag       ENUMERATION contained nextgroup=uncrustify_EnumerationBody skipwhite skipnl
syntax region  uncrustify_EnumerationBody      matchgroup=uncrustify_Brace start=/{/ end=/}/ transparent contained contains=uncrustify_EnumerationConstants,uncrustify_Comma
syntax keyword uncrustify_EnumerationConstants AUTO CR LF CRLF IGNORE ADD REMOVE FORCE JOIN LEAD LEAD_BREAK LEAD_FORCE TRAIL TRAIL_BREAK TRAIL_FORCE

highlight default link uncrustify_EnumerationTag       Type
highlight default link uncrustify_EnumerationConstants Constant

syntax match uncrustify_Comment /#.*$/ contains=uncrustify_DocSection,uncrustify_DocBriefSection,uncrustify_CommentEmphasis,@Spell

highlight default link uncrustify_Comment Comment

syntax region uncrustify_CommentEmphasis start=/`/ end=/`/ contained oneline
syntax region uncrustify_CommentEmphasis start=/</ end=/>/ contained oneline

highlight default link uncrustify_CommentEmphasis Underlined

syntax region uncrustify_DocSection start=/[^#]*#\s*@\l\+\(\s\+\|$\)/ms=s end=/\(^[^#]*#\s*@\|^[^#]*#\s*$\|^[^#]*$\)/me=s-1 transparent contained contains=uncrustify_DocTag,uncrustify_DocTypeTag,uncrustify_DocDefaultTag,uncrustify_CommentEmphasis,@Spell
syntax match  uncrustify_DocTag     /#\s*\zs@\l\+\ze\(\s\+\|$\)/ contained

highlight default link uncrustify_DocTag SpecialComment

syntax match uncrustify_DocBriefTag   /#\s*\zs@brief/ contained nextgroup=uncrustify_DocBriefText skipwhite skipnl
syntax match uncrustify_DocTypeTag    /#\s*\zs@type\ze\s\+/ contained nextgroup=uncrustify_BooleanTag,uncrustify_IntegerTag,uncrustify_StringTag,uncrustify_EnumerationTag skipwhite skipnl
syntax match uncrustify_DocDefaultTag /#\s*\zs@default\ze\s\+/  contained nextgroup=uncrustify_BooleanConstants,uncrustify_IntegerConstants,uncrustify_StringConstants,uncrustify_EnumerationConstants skipwhite skipnl

highlight default link uncrustify_DocBriefTag   uncrustify_DocTag
highlight default link uncrustify_DocTypeTag    uncrustify_DocTag
highlight default link uncrustify_DocDefaultTag uncrustify_DocTag

syntax region uncrustify_DocBriefSection start=/[^#]*#\s*@brief\(\s\+\|$\)/ms=s end=/\(^[^#]*#\s*@\|^[^#]*#\s*$\|^[^#]*$\)/me=s-1 transparent contained contains=uncrustify_DocBriefTag,uncrustify_DocBriefText
syntax match  uncrustify_DocBriefText    /\(#\s*@brief\s\+\|\%(#\s*@brief\)\@!#\s*\)\@<=\zs.*$/ contained contains=uncrustify_CommentEmphasis,@Spell

highlight link uncrustify_DocBriefText Statement

let b:current_syntax = 'uncrustify'

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
