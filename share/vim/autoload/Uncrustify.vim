" Preamble {{{
" ------------------------------------------------------------------------------
"        File: Uncrustify.vim
" ------------------------------------------------------------------------------
"     Version: 0.0.0
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ------------------------------------------------------------------------------
" }}} Preamble

if exists('g:Uncrustify#autoload')
  finish
endif

let g:Uncrustify#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ------------------------------------------------------------------------------
" Public {{{
" ------------------------------------------------------------------------------
function! Uncrustify#FormatRange() range
  call s:Format(a:firstline, a:lastline)
endfunction

function! Uncrustify#FormatBuffer()
  call s:Format(1, '$')
endfunction

function! Uncrustify#CreateCommand(executable_path,
\                                  configuration_path,
\                                  language,
\                                  fragmented,
\                                  quiet)
  let l:command =
  \ shellescape(simplify(expand(a:executable_path)))                           .
  \ ' '                                                                        .
  \ '-c'                                                                       .
  \ ' '                                                                        .
  \ shellescape(simplify(expand(a:configuration_path)))                        .
  \ ' '                                                                        .
  \ '-l'                                                                       .
  \ ' '                                                                        .
  \ a:language
  if a:fragmented
    let l:command .=
    \ ' '                                                                      .
    \ '--frag'
  endif
  if a:quiet
    let l:command .=
    \ ' '                                                                      .
    \ '-q'
  else
    let l:command .=
    \ ' '                                                                      .
    \ '-L1-2'
  endif
  return l:command
endfunction
" ------------------------------------------------------------------------------
" }}} Public

" Private {{{
" ------------------------------------------------------------------------------
function! s:Format(from, to)
  let l:input_buffer  = bufnr('%')
  let l:output_buffer = bufnr('Uncrustify', 1)
  let l:view          = winsaveview()
  let l:position      = getpos('.')
  let l:shellredir    = &l:shellredir
  let l:lazyredraw    = &l:lazyredraw
  let l:syntax        = &l:syntax
  try
    call s:Run(l:input_buffer, l:output_buffer, a:from, a:to)
  catch
    throw v:exception
  finally
    silent! noautocmd execute l:input_buffer  . 'buffer!'
    silent! noautocmd execute l:output_buffer . 'bwipeout!'
    call winrestview(l:view)
    call setpos('.', l:position)
    let &l:shellredir = l:shellredir
    let &l:lazyredraw = l:lazyredraw
    let &l:syntax     = l:syntax
  endtry
endfunction

function! s:Run(input_buffer, output_buffer, from, to)
  if !Uncrustify#Executable#Exists()
    throw "Uncrustify: Error: Cannot find executable."
  endif
  if !Uncrustify#Configuration#Exists()
    throw "Uncrustify: Error: Cannot find configuration."
  endif
  let l:from = line(a:from)
  if l:from == 0
    let l:from = a:from
  endif
  let l:to = line(a:to)
  if l:to == 0
    let l:to = a:to
  endif
  let l:count = l:to - l:from + 1
  let l:range = l:from . ',' . l:to
  " ----------------------------------------------------------------------------
  let l:lines              = getbufline(a:input_buffer, from, to)
  let l:executable_path    = Uncrustify#Executable#GetPath()
  let l:configuration_path = Uncrustify#Configuration#GetAbsolutePath()
  let l:language           = Uncrustify#Languages#Get(&l:filetype)
  let l:fragmented         = (l:from == 1 && l:to == line('$')) ? 0 : 1
  " ----------------------------------------------------------------------------
  let &l:lazyredraw        = 1
  silent! noautocmd execute a:output_buffer . 'buffer!'
  let &l:swapfile = 0
  let l:error =
  \ s:Filter(
  \   l:lines,
  \   l:executable_path,
  \   l:configuration_path,
  \   l:language,
  \   l:fragmented,
  \   1
  \ )
  if l:error
    call
    \ s:Filter(
    \   l:lines,
    \   l:executable_path,
    \   l:configuration_path,
    \   l:language,
    \   l:fragmented,
    \   0
    \ )
  endif
  silent! noautocmd execute a:input_buffer . 'buffer!'
  let l:lines = getbufline(a:output_buffer, 1, '$')
  if l:error
    throw
    \ "Uncrustify: Error:"                                                     .
    \ ' '                                                                      .
    \ join(l:lines, ' | ')
  endif
  silent! noautocmd execute l:to    . 'put=l:lines'
  silent! noautocmd execute l:range . 'delete_'
  echomsg
  \ "Uncrustify:"                                                              .
  \ ' '                                                                        .
  \ l:count                                                                    .
  \ ' '                                                                        .
  \ (l:count == 1 ? "line" : "lines")                                          .
  \ ' '                                                                        .
  \ "formatted."
endfunction

function! s:Filter(lines,
\                  executable_path,
\                  configuration_path,
\                  language,
\                  fragmented,
\                  quiet)
  silent! noautocmd %delete_
  silent! noautocmd 1put=a:lines
  silent! noautocmd 1delete_
  if a:quiet
    let &l:shellredir = '1>%s'
  else
    let &l:shellredir = '2>%s'
  endif
  silent! noautocmd execute
  \ '%'                                                                        .
  \ '!'                                                                        .
  \ Uncrustify#CreateCommand(
  \   a:executable_path,
  \   a:configuration_path,
  \   a:language,
  \   a:fragmented,
  \   a:quiet
  \ )
  return v:shell_error
endfunction
" ------------------------------------------------------------------------------
" }}} Private
" ------------------------------------------------------------------------------
" }}} Functions

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
