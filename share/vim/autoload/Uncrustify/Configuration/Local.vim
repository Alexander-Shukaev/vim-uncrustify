" Preamble {{{
" ------------------------------------------------------------------------------
"        File: Local.vim
" ------------------------------------------------------------------------------
"     Version: 0.0.0
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ------------------------------------------------------------------------------
" }}} Preamble

if exists('g:Uncrustify#Configuration#Local#autoload')
  finish
endif

let g:Uncrustify#Configuration#Local#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ------------------------------------------------------------------------------
" Public {{{
" ------------------------------------------------------------------------------
function! Uncrustify#Configuration#Local#SetName(name)
  if type(a:name) != type('')
    throw "Uncrustify: Error: `a:name` should be of type `String`."
  endif
  let s:name = a:name
endfunction

function! Uncrustify#Configuration#Local#GetName()
  return s:name
endfunction

function! Uncrustify#Configuration#Local#GetPath()
  return findfile(Uncrustify#Configuration#Local#GetName(), '%,.;')
endfunction

function! Uncrustify#Configuration#Local#GetAbsolutePath()
  return fnamemodify(Uncrustify#Configuration#Local#GetPath(), ':p')
endfunction

function! Uncrustify#Configuration#Local#IsAbsolute()
  return
  \ Uncrustify#Configuration#Local#GetPath()
  \ ==
  \ Uncrustify#Configuration#Local#GetAbsolutePath()
endfunction

function! Uncrustify#Configuration#Local#Exists()
  return filereadable(Uncrustify#Configuration#Local#GetPath())
endfunction
" ------------------------------------------------------------------------------
" }}} Public
" ------------------------------------------------------------------------------
" }}} Functions

" Defaults {{{
" ------------------------------------------------------------------------------
call Uncrustify#Configuration#Local#SetName('.uncrustify')
" ------------------------------------------------------------------------------
" }}} Defaults

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
