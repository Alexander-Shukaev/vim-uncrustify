" Preamble {{{
" ------------------------------------------------------------------------------
"        File: Configuration.vim
" ------------------------------------------------------------------------------
"     Version: 0.0.0
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ------------------------------------------------------------------------------
" }}} Preamble

if exists('g:Uncrustify#Configuration#autoload')
  finish
endif

let g:Uncrustify#Configuration#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ------------------------------------------------------------------------------
" Public {{{
" ------------------------------------------------------------------------------
function! Uncrustify#Configuration#GetPath()
  if     Uncrustify#Configuration#Local#Exists()
    return Uncrustify#Configuration#Local#GetPath()
  elseif Uncrustify#Configuration#Global#Exists()
    return Uncrustify#Configuration#Global#GetPath()
  elseif Uncrustify#Configuration#System#Exists()
    return Uncrustify#Configuration#System#GetPath()
  else
    return ''
  endif
endfunction

function! Uncrustify#Configuration#GetAbsolutePath()
  if     Uncrustify#Configuration#Local#Exists()
    return Uncrustify#Configuration#Local#GetAbsolutePath()
  elseif Uncrustify#Configuration#Global#Exists()
    return Uncrustify#Configuration#Global#GetAbsolutePath()
  elseif Uncrustify#Configuration#System#Exists()
    return Uncrustify#Configuration#System#GetAbsolutePath()
  else
    return ''
  endif
endfunction

function! Uncrustify#Configuration#Exists()
  return
  \ Uncrustify#Configuration#Local#Exists()
  \ ||
  \ Uncrustify#Configuration#Global#Exists()
  \ ||
  \ Uncrustify#Configuration#System#Exists()
endfunction

function! Uncrustify#Configuration#Check()
  if Uncrustify#Configuration#Exists()
    redraw
    echomsg
    \ "Uncrustify: Using configuration:"                                       .
    \ ' '                                                                      .
    \ '"'                                                                      .
    \ Uncrustify#Configuration#GetAbsolutePath()                               .
    \ '"'                                                                      .
    \ '.'
  else
    echohl WarningMsg
    echomsg "Uncrustify: Warning: Cannot find configuration."
    echohl None
  endif
endfunction
" ------------------------------------------------------------------------------
" }}} Public
" ------------------------------------------------------------------------------
" }}} Functions

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
