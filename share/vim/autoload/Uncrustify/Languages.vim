" Preamble {{{
" ------------------------------------------------------------------------------
"        File: Languages.vim
" ------------------------------------------------------------------------------
"     Version: 0.0.0
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ------------------------------------------------------------------------------
" }}} Preamble

if exists('g:Uncrustify#Languages#autoload')
  finish
endif

let g:Uncrustify#Languages#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ------------------------------------------------------------------------------
" Public {{{
" ------------------------------------------------------------------------------
function! Uncrustify#Languages#Set(filetype, language)
  if type(a:filetype) != type('')
    throw "Uncrustify: Error: `a:filetype` should be of type `String`."
  endif
  if type(a:language) != type('')
    throw "Uncrustify: Error: `a:language` should be of type `String`."
  endif
  let s:languages[a:filetype] = a:language
endfunction

function! Uncrustify#Languages#Remove(filetype)
  if type(a:filetype) != type('')
    throw "Uncrustify: Error: `a:filetype` should be of type `String`."
  endif
  try
    call remove(s:languages, a:filetype)
  catch
  endtry
endfunction

function! Uncrustify#Languages#Get(filetype)
  if type(a:filetype) != type('')
    throw "Uncrustify: Error: `a:filetype` should be of type `String`."
  endif
  let l:language = get(s:languages, a:filetype, '')
  if empty(l:language)
    throw
    \ "Uncrustify: Error: Language for the corresponding filetype not found:"  .
    \ ' '                                                                      .
    \ "`a:filetype` is"                                                        .
    \ " "                                                                      .
    \ '"'                                                                      .
    \ a:filetype                                                               .
    \ '"'                                                                      .
    \ '.'
  endif
  return l:language
endfunction
" ------------------------------------------------------------------------------
" }}} Public
" ------------------------------------------------------------------------------
" }}} Functions

" Defaults {{{
" ------------------------------------------------------------------------------
let s:languages = {}

call Uncrustify#Languages#Set('c',      'c')
call Uncrustify#Languages#Set('cpp',    'cpp')
call Uncrustify#Languages#Set('cs',     'cs')
call Uncrustify#Languages#Set('d',      'd')
call Uncrustify#Languages#Set('java',   'java')
call Uncrustify#Languages#Set('objc',   'oc')
call Uncrustify#Languages#Set('objcpp', 'oc+')
" ------------------------------------------------------------------------------
" }}} Defaults

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
